//
//  LinearLayoutView.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/7.
//

import Foundation
import UIKit

@IBDesignable
class LinearLayoutView: UIView {
    private var contentViewWidthConstraint: NSLayoutConstraint?
    private var contentViewHeightConstraint: NSLayoutConstraint?
    
    var layout: LinearLayout?
    
    var scrollView: UIScrollView
    var contentView: View
    var removingContentView: UIView
    
    @IBInspectable var layoutOption: Int = 0 {
        didSet {
            refresh()
        }
    }

    @IBInspectable var maxSize: CGFloat = 100 {
        didSet {
            refresh()
        }
    }
    
    required init?(coder: NSCoder) {
        removingContentView = UIView()
        scrollView = ScrollView(frame: .zero)
        contentView = View(frame: .zero)
        super.init(coder: coder)
        initUI()
        layoutOption = 0
        refresh()
    }
    
    override init(frame: CGRect) {
        removingContentView = UIView()
        scrollView = ScrollView(frame: .zero)
        contentView = View(frame: .zero)
        super.init(frame: frame)
        initUI()
        layoutOption = 0
        refresh()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let views = subviews.filter { $0 !== scrollView && $0 !== removingContentView && $0 !== removingContentView }
        if !views.isEmpty {
            views.forEach { $0.removeFromSuperview() }
            views.forEach {
                $0.frame = CGRect(x: 0, y: 0, width: $0.frame.width, height: $0.frame.height)
                addArrangedSubview($0, animated: false)
            }
        }
        layout?.layoutSubviews()
    }
    
    func addArrangedSubview(_ view: UIView, animated: Bool) {
        guard let layout = layout else { return }
        layout.add(view: view, animated: animated)
        scrollView.scrollRectToVisible(contentView.convert(view.frame, to: scrollView), animated: true)
    }
    
    func addArrangedSubview(_ view: UIView, at index: Int, animated: Bool) {
        guard let layout = layout else { return }
        layout.add(view: view, at: index, animated: animated)
        scrollView.scrollRectToVisible(contentView.convert(view.frame, to: scrollView), animated: true)
    }
    
    func removeArrangedSubview(at index: Int, animated: Bool) {
        guard let layout = layout else { return }
        layout.remove(at: index, animated: animated)
    }
    
    private func initUI() {
        removingContentView.backgroundColor = .cyan
        removingContentView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(removingContentView)
        removingContentView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        removingContentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        removingContentView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        removingContentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        contentView.backgroundColor = .clear
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        contentViewWidthConstraint = contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        contentViewWidthConstraint?.isActive = true
        contentViewHeightConstraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        contentViewHeightConstraint?.isActive = true
    }

    private func refresh() {
        layout?.willRemoveFromSuperview()
        let views = contentView.subviews
        views.forEach { $0.removeFromSuperview() }
        
        
        switch layoutOption {
            case 0:
                layout = VerticalLayout(layoutView: self, scrollView: scrollView, contentView: contentView, removingContentView: removingContentView)
                contentViewWidthConstraint?.isActive = true
                contentViewHeightConstraint?.isActive = false
            case 1:
                layout = HorizontalLayout(layoutView: self, scrollView: scrollView, contentView: contentView, removingContentView: removingContentView)
                contentViewWidthConstraint?.isActive = false
                contentViewHeightConstraint?.isActive = true
            case 2:
                layout = WrapVerticalLayout(layoutView: self, scrollView: scrollView, contentView: contentView, removingContentView: removingContentView, maxSize: maxSize)
                contentViewWidthConstraint?.isActive = true
                contentViewHeightConstraint?.isActive = false
            default:
                
                layout = WrapHorizontalLayout(layoutView: self, scrollView: scrollView, contentView: contentView, removingContentView: removingContentView, maxSize: maxSize)
                contentViewWidthConstraint?.isActive = false
                contentViewHeightConstraint?.isActive = true
        }
        views.forEach { layout?.add(view: $0, animated: false)}
        contentView.setNeedsLayout()
        scrollView.setNeedsLayout()
        contentView.invalidateIntrinsicContentSize()
        scrollView.invalidateIntrinsicContentSize()
        setNeedsLayout()
        layoutIfNeeded()
    }
}
