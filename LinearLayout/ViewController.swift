//
//  ViewController.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/6.
//

import UIKit

class ViewController: UIViewController {
    enum Orientation {
        case horizontal
        case vertical
    }

    enum ContentMode {
        case wrap
        case fix
    }

    @IBOutlet private var linearLayoutView: LinearLayoutView!
    @IBOutlet private var linearLayoutHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var linearLayoutTrailingConstraint: NSLayoutConstraint!
    @IBOutlet private var hintLabel: UILabel!
    @IBOutlet private var anchorView: UIView!
    private var innerLayout: LinearLayoutView!
    private let colors: [UIColor] = [.red, .blue, .green, .yellow, .cyan]
    private var counter = 0
    private var previousPoint: CGPoint = .zero
    private var panGesture: UIPanGestureRecognizer?
    private var orientation: Orientation = .vertical
    private var contentMode: ContentMode = .fix

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeDown.direction = .down
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeUp.direction = .up
        linearLayoutView.addGestureRecognizer(tap)
        linearLayoutView.addGestureRecognizer(swipeDown)
        linearLayoutView.addGestureRecognizer(swipeUp)

        let pan = UIPanGestureRecognizer(target: self, action: #selector(panAction(_:)))
        anchorView.addGestureRecognizer(pan)
        panGesture = pan
    }

    @IBAction func add() {
        let count = linearLayoutView.contentView.subviews.count
        let color = colors[(count) % colors.count]
        let view = UINib(nibName: "LabelContentView", bundle: .main).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.backgroundColor = color
        linearLayoutView.addArrangedSubview(view, animated: true)
    }

    @IBAction func remove() {
        linearLayoutView.removeArrangedSubview(at: 0, animated: true)
        linearLayoutView.removeArrangedSubview(at: 0, animated: true)
//        add()
        let count = linearLayoutView.contentView.subviews.count
        let color = colors[(count) % colors.count]
        let view = UINib(nibName: "LabelContentView", bundle: .main).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.backgroundColor = color
        linearLayoutView.addArrangedSubview(view, at: 0, animated: true)
    }

    @objc func tapAction(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: linearLayoutView)
        guard let index = (linearLayoutView.contentView.subviews.firstIndex(where: {
            $0.frame.contains(location)
        })) else { return }

        linearLayoutView.removeArrangedSubview(at: index, animated: true)
    }

    @objc func swipeAction(_ gesture: UISwipeGestureRecognizer) {
        let location = gesture.location(in: linearLayoutView)
        guard let index = (linearLayoutView.contentView.subviews.firstIndex(where: {
            $0.frame.contains(location)
        })) else { return }

        let count = linearLayoutView.contentView.subviews.count
        let color = colors[(count) % colors.count]
        let view = UINib(nibName: "LabelContentView", bundle: .main).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.backgroundColor = color

        if gesture.direction == .down {
            linearLayoutView.addArrangedSubview(view, at: index + 1, animated: true)
        } else if gesture.direction == .up {
            linearLayoutView.addArrangedSubview(view, at: index, animated: true)
        }
    }

    @objc func panAction(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
            case .began:
                previousPoint = gestureRecognizer.location(in: view)
            case .changed:
                let currentPoint = gestureRecognizer.location(in: view)
                let offset = currentPoint.y - previousPoint.y
                previousPoint = currentPoint
                linearLayoutHeightConstraint.constant += offset
            default:
                return
        }
    }

    @IBAction func layoutChange(_ segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            contentMode = .fix
            panGesture?.isEnabled = true
            hintLabel.isHidden = false
        } else {
            contentMode = .wrap
            panGesture?.isEnabled = false
            hintLabel.isHidden = true
        }
        refreshLayout()
    }

    @IBAction func orientationChange(_ segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            orientation = .vertical
        } else {
            orientation = .horizontal
        }

        refreshLayout()
    }

    private func refreshLayout() {
        linearLayoutHeightConstraint.isActive = false
        linearLayoutTrailingConstraint.isActive = false
        if contentMode == .fix && orientation == .vertical {
            linearLayoutView.layoutOption = 0
            linearLayoutHeightConstraint.isActive = true
            linearLayoutTrailingConstraint.isActive = true
        } else if contentMode == .fix && orientation == .horizontal {
            linearLayoutView.layoutOption = 1
            linearLayoutTrailingConstraint.isActive = true
            linearLayoutHeightConstraint.isActive = true
        } else if contentMode == .wrap && orientation == .vertical {
            linearLayoutView.layoutOption = 2
            linearLayoutView.maxSize = 320
            linearLayoutTrailingConstraint.isActive = true
        } else if contentMode == .wrap && orientation == .horizontal {
            linearLayoutView.layoutOption = 3
            linearLayoutView.maxSize = view.frame.width - 100
            linearLayoutHeightConstraint.isActive = true
        }
    }
}
