//
//  VerticalLayout.swift.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/7.
//

import Foundation
import UIKit

class WrapVerticalLayout: LinearLayout {
    private let layoutView: UIView
    private let scrollView: UIScrollView
    private let contentView: View
    private let removingContentView: UIView
    private var heightConstraints: [NSLayoutConstraint]
    private var topConstraints: [NSLayoutConstraint]
    private var animator: UIViewPropertyAnimator
    private let duration: CGFloat = 0.25
    private var bottomConstraint: NSLayoutConstraint?
    private var maxSizeConstraint: NSLayoutConstraint?
    
    init(layoutView: UIView, scrollView: UIScrollView, contentView: View, removingContentView: UIView, maxSize: CGFloat) {
        self.layoutView = layoutView
        self.scrollView = scrollView
        self.contentView = contentView
        self.removingContentView = removingContentView
        
        maxSizeConstraint = scrollView.heightAnchor.constraint(lessThanOrEqualToConstant: maxSize)
        maxSizeConstraint?.isActive = true
        
        contentView.isDynamic = true
        animator = UIViewPropertyAnimator(duration: duration, curve: .linear, animations: nil)
        heightConstraints = [NSLayoutConstraint]()
        topConstraints = [NSLayoutConstraint]()
    }
    
    func add(view: UIView, animated: Bool) {
        add(view: view, at: contentView.subviews.count, animated: animated)
    }
    
    func add(view: UIView, at index: Int, animated: Bool) {
        view.frame = CGRect(x: 0, y: contentView.frame.height - view.frame.height, width: contentView.frame.width, height: view.frame.height)
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.insertSubview(view, at: index)
        arrangeLayerZ(views: contentView.subviews)
        
        let heightConstraint = view.heightAnchor.constraint(equalToConstant: view.frame.height)
        heightConstraint.isActive = true
        heightConstraints.insert(heightConstraint, at: index)
        
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        
        bottomConstraint?.isActive = false
        if let lastView = contentView.subviews.last {
            let bottomConstraint = lastView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            bottomConstraint.isActive = true
            self.bottomConstraint = bottomConstraint
        }
        
        
        let initialOffset: CGFloat
        if index == 0 {
            initialOffset = -view.frame.height
        } else {
            initialOffset = contentView.subviews[0 ..< index - 1].reduce(0) { $0 + $1.frame.height }
        }
        
        let constraint = view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: initialOffset)
        constraint.isActive = true
        topConstraints.insert(constraint, at: index)
        
        contentView.layoutIfNeeded()
        
        for i in 0 ..< topConstraints.count {
            if i == 0 {
                topConstraints[i].constant = 0
            } else {
                let topOffset = contentView.subviews[0 ..< i].reduce(0) { $0 + $1.frame.height }
                topConstraints[i].constant = topOffset
            }
        }

        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }

        if animated {
            animator.addAnimations {
                action()
            }
            animator.startAnimation()
        } else {
            action()
        }
    }
    
    func remove(at index: Int, animated: Bool) {
        guard !contentView.subviews.isEmpty,
              index >= 0 && index < contentView.subviews.count else {
                  return
              }
        
        let removeViewTopConstraint = topConstraints.remove(at: index)
        let topConstant = removeViewTopConstraint.constant
        
        let removeView = contentView.subviews[index]
        removeView.removeFromSuperview()
        heightConstraints.remove(at: index)
        
        for i in 0 ..< topConstraints.count {
            if i == 0 {
                topConstraints[i].constant = 0
            } else {
                let topOffset = contentView.subviews[0 ..< i].reduce(0) { $0 + $1.frame.height }
                topConstraints[i].constant = topOffset
            }
        }
        
        removingContentView.addSubview(removeView)
        removeView.leadingAnchor.constraint(equalTo: removingContentView.leadingAnchor).isActive = true
        removeView.trailingAnchor.constraint(equalTo: removingContentView.trailingAnchor).isActive = true
        let newRemoveViewTopConstraint = removeView.topAnchor.constraint(equalTo: removingContentView.topAnchor, constant: topConstant)
        newRemoveViewTopConstraint.isActive = true
        newRemoveViewTopConstraint.constant -= removeView.frame.height
        
        bottomConstraint?.isActive = false
        if let lastView = contentView.subviews.last {
            let bottomConstraint = lastView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            bottomConstraint.isActive = true
            self.bottomConstraint = bottomConstraint
        }
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        let completion = {
            removeView.removeFromSuperview()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.addCompletion { _ in
                completion()
            }
            
            animator.startAnimation()
        } else {
            action()
            completion()
        }
    }
    
    func layoutSubviews() {
    }
    
    func willRemoveFromSuperview() {
        maxSizeConstraint?.isActive = false
        heightConstraints.forEach { $0.isActive = false }
    }
    
    private func arrangeLayerZ(views: [UIView]) {
        var count = views.count
        views.forEach {
            $0.layer.zPosition = CGFloat(count)
            count -= 1
        }
    }
}
