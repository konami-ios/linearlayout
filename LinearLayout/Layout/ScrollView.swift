//
//  ScrollView.swift.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/18.
//

import Foundation
import UIKit

class ScrollView: UIScrollView {
    override var intrinsicContentSize: CGSize {
        return subviews.first?.intrinsicContentSize ?? .zero
    }
}
