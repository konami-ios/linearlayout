//
//  HorizontalLayout.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/17.
//

import Foundation
import UIKit

class HorizontalLayout: LinearLayout {
    private let layoutView: UIView
    private let scrollView: UIScrollView
    private let contentView: View
    private let removingContentView: UIView
    private var widthConstraints: [NSLayoutConstraint]
    private var leadingConstraints: [NSLayoutConstraint]
    private var animator: UIViewPropertyAnimator
    private let duration: CGFloat = 0.25
    init(layoutView: UIView, scrollView: UIScrollView, contentView: View, removingContentView: UIView) {
        self.layoutView = layoutView
        self.scrollView = scrollView
        self.contentView = contentView
        self.removingContentView = removingContentView
        
        contentView.isDynamic = false
        animator = UIViewPropertyAnimator(duration: duration, curve: .linear, animations: nil)
        widthConstraints = [NSLayoutConstraint]()
        leadingConstraints = [NSLayoutConstraint]()
    }
    
    func add(view: UIView, animated: Bool) {
        add(view: view, at: contentView.subviews.count, animated: animated)
    }
    
    func add(view: UIView, at index: Int, animated: Bool) {
        let newWidth = layoutView.frame.width / CGFloat(contentView.subviews.count + 1)
        view.frame = CGRect(x: contentView.frame.width - view.frame.width, y: 0, width: newWidth, height: layoutView.frame.height)
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.insertSubview(view, at: index)
        arrangeLayerZ(views: contentView.subviews)
        
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        let widthConstraint = view.widthAnchor.constraint(equalToConstant: newWidth)
        widthConstraint.isActive = true
        widthConstraints.append(widthConstraint)
        
        let initialOffset = newWidth * CGFloat(index - 1)
        let constraint = view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: initialOffset)
        constraint.isActive = true
        leadingConstraints.insert(constraint, at: index)
        
        contentView.layoutIfNeeded()
        
        for i in 0 ..< leadingConstraints.count {
            leadingConstraints[i].constant = newWidth * CGFloat(i)
        }
        widthConstraints.forEach { $0.constant = newWidth }
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.startAnimation()
        } else {
            action()
        }
    }
    
    func remove(at index: Int, animated: Bool) {
        guard !contentView.subviews.isEmpty,
              index >= 0 && index < contentView.subviews.count else {
                  return
              }
        
        let remoeViewLeadingConstraint = leadingConstraints.remove(at: index)
        let leadingConstant = remoeViewLeadingConstraint.constant
        
        let removeView = contentView.subviews[index]
        removeView.removeFromSuperview()
        
        let newWidth = layoutView.frame.width / CGFloat(contentView.subviews.count)
        
        widthConstraints.remove(at: index)
        widthConstraints.forEach { $0.constant = newWidth }
        
        for i in 0 ..< leadingConstraints.count {
            leadingConstraints[i].constant = newWidth * CGFloat(i)
        }
        
        removingContentView.addSubview(removeView)
        removeView.topAnchor.constraint(equalTo: removingContentView.topAnchor).isActive = true
        removeView.bottomAnchor.constraint(equalTo: removingContentView.bottomAnchor).isActive = true
        let newRemoveViewLeadingConstraint = removeView.leadingAnchor.constraint(equalTo: removingContentView.leadingAnchor, constant: leadingConstant)
        newRemoveViewLeadingConstraint.isActive = true
        newRemoveViewLeadingConstraint.constant -= removeView.frame.width
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        let completion = {
            removeView.removeFromSuperview()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.addCompletion { _ in
                completion()
            }
            
            animator.startAnimation()
        } else {
            action()
            completion()
        }
    }
    
    func layoutSubviews() {
        guard !contentView.subviews.isEmpty else { return }
        
        let height = layoutView.frame.width / CGFloat(contentView.subviews.count)
        widthConstraints.forEach{ $0.constant = height }
        
        for i in 0 ..< leadingConstraints.count {
            leadingConstraints[i].constant = height * CGFloat(i)
        }
        
        contentView.layoutIfNeeded()
    }
    
    func willRemoveFromSuperview() {
        widthConstraints.forEach { $0.isActive = false }
    }
    
    private func arrangeLayerZ(views: [UIView]) {
        var count = views.count
        views.forEach {
            $0.layer.zPosition = CGFloat(count)
            count -= 1
        }
    }
}
