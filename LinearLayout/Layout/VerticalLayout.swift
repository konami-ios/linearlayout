//
//  VerticalLayout.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/12.
//

import Foundation
import UIKit

class VerticalLayout: LinearLayout {
    private let layoutView: UIView
    private let scrollView: UIScrollView
    private let contentView: View
    private let removingContentView: UIView
    private var heightConstraints: [NSLayoutConstraint]
    private var topConstraints: [NSLayoutConstraint]
    private var animator: UIViewPropertyAnimator
    private let duration: CGFloat = 0.25
    init(layoutView: UIView, scrollView: UIScrollView, contentView: View, removingContentView: UIView) {
        self.layoutView = layoutView
        self.scrollView = scrollView
        self.contentView = contentView
        self.removingContentView = removingContentView
        
        contentView.isDynamic = false
        animator = UIViewPropertyAnimator(duration: duration, curve: .linear, animations: nil)
        heightConstraints = [NSLayoutConstraint]()
        topConstraints = [NSLayoutConstraint]()
    }
    
    func add(view: UIView, animated: Bool) {
        add(view: view, at: contentView.subviews.count, animated: animated)
    }
    
    func add(view: UIView, at index: Int, animated: Bool) {
        let newHeight = layoutView.frame.height / CGFloat(contentView.subviews.count + 1)
        
        view.frame = CGRect(x: 0, y: layoutView.frame.height - view.frame.height, width: layoutView.frame.width, height: newHeight)
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.insertSubview(view, at: index)
        arrangeLayerZ(views: contentView.subviews)
        
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        
        let heightConstraint = view.heightAnchor.constraint(equalToConstant: newHeight)
        heightConstraint.isActive = true
        heightConstraints.append(heightConstraint)

        let initialOffset = newHeight * CGFloat(index - 1)
        let constraint = view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: initialOffset)
        constraint.isActive = true
        topConstraints.insert(constraint, at: index)
        
        contentView.layoutIfNeeded()
        
        for i in 0 ..< topConstraints.count {
            topConstraints[i].constant = newHeight * CGFloat(i)
        }
        heightConstraints.forEach { $0.constant = newHeight }
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.startAnimation()
        } else {
            action()
        }
    }
    
    func remove(at index: Int, animated: Bool) {
        guard !contentView.subviews.isEmpty,
              index >= 0 && index < contentView.subviews.count else {
                  return
              }
        
        let remoeViewTopConstraint = topConstraints.remove(at: index)
        let topConstant = remoeViewTopConstraint.constant
        
        let removeView = contentView.subviews[index]
        removeView.removeFromSuperview()
        
        let newHeight = layoutView.frame.height / CGFloat(contentView.subviews.count)
        
        heightConstraints.remove(at: index)
        heightConstraints.forEach { $0.constant = newHeight }
        
        for i in 0 ..< topConstraints.count {
            topConstraints[i].constant = newHeight * CGFloat(i)
        }
        
        removingContentView.addSubview(removeView)
        removeView.leadingAnchor.constraint(equalTo: removingContentView.leadingAnchor).isActive = true
        removeView.trailingAnchor.constraint(equalTo: removingContentView.trailingAnchor).isActive = true
        let newRemoveViewTopConstraint = removeView.topAnchor.constraint(equalTo: removingContentView.topAnchor, constant: topConstant)
        newRemoveViewTopConstraint.isActive = true
        newRemoveViewTopConstraint.constant -= removeView.frame.height
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        let completion = {
            removeView.removeFromSuperview()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.addCompletion { _ in
                completion()
            }
            
            animator.startAnimation()
        } else {
            action()
            completion()
        }
    }
    
    func layoutSubviews() {
        guard !contentView.subviews.isEmpty else { return }
        
        let height = layoutView.frame.height / CGFloat(contentView.subviews.count)
        heightConstraints.forEach{ $0.constant = height }
        
        for i in 0 ..< topConstraints.count {
            topConstraints[i].constant = height * CGFloat(i)
        }
        
        contentView.layoutIfNeeded()
    }
    
    func willRemoveFromSuperview() {
        heightConstraints.forEach { $0.isActive = false }
    }
    
    private func arrangeLayerZ(views: [UIView]) {
        var count = views.count
        views.forEach {
            $0.layer.zPosition = CGFloat(count)
            count -= 1
        }
    }
}
