//
//  LinearLayout.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/11.
//

import Foundation
import UIKit

@objc
protocol LinearLayout: AnyObject {
    func add(view: UIView, animated: Bool)
    func add(view: UIView, at index: Int, animated: Bool)
    func remove(at index: Int, animated: Bool)
    func willRemoveFromSuperview()
    func layoutSubviews()
}
