//
//  WrapHorizontalLayout.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/17.
//

import Foundation
import UIKit

class WrapHorizontalLayout: LinearLayout {
    private let layoutView: UIView
    private let scrollView: UIScrollView
    private let contentView: View
    private let removingContentView: UIView
    private var widthConstraints: [NSLayoutConstraint]
    private var leadingConstraints: [NSLayoutConstraint]
    private var animator: UIViewPropertyAnimator
    private let duration: CGFloat = 0.25
    private var trailingConstraint: NSLayoutConstraint?
    private var maxSizeConstraint: NSLayoutConstraint?
    init(layoutView: UIView, scrollView: UIScrollView, contentView: View, removingContentView: UIView, maxSize: CGFloat) {
        self.layoutView = layoutView
        self.scrollView = scrollView
        self.contentView = contentView
        self.removingContentView = removingContentView
        
        maxSizeConstraint = scrollView.widthAnchor.constraint(lessThanOrEqualToConstant: maxSize)
        maxSizeConstraint?.isActive = true
        
        contentView.isDynamic = true
        animator = UIViewPropertyAnimator(duration: duration, curve: .linear, animations: nil)
        widthConstraints = [NSLayoutConstraint]()
        leadingConstraints = [NSLayoutConstraint]()
    }
    
    func add(view: UIView, animated: Bool) {
        add(view: view, at: contentView.subviews.count, animated: animated)
    }
    
    func add(view: UIView, at index: Int, animated: Bool) {
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: contentView.frame.height)
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.insertSubview(view, at: index)
        arrangeLayerZ(views: contentView.subviews)
        
        let widthConstraint = view.widthAnchor.constraint(equalToConstant: view.frame.width)
        widthConstraint.isActive = true
        widthConstraints.insert(widthConstraint, at: index)
        
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        trailingConstraint?.isActive = false
        if let lastView = contentView.subviews.last {
            let trailingConstraint = lastView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
            trailingConstraint.isActive = true
            self.trailingConstraint = trailingConstraint
        }
        
        let initialOffset: CGFloat
        if index == 0 {
            initialOffset = -view.frame.width
        } else {
            initialOffset = contentView.subviews[0 ..< index - 1].reduce(0) { $0 + $1.frame.width }
        }
        
        let constraint = view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: initialOffset)
        constraint.isActive = true
        leadingConstraints.insert(constraint, at: index)
        contentView.layoutIfNeeded()
        
        for i in 0 ..< leadingConstraints.count {
            if i == 0 {
                leadingConstraints[i].constant = 0
            } else {
                let topOffset = contentView.subviews[0 ..< i].reduce(0) { $0 + $1.frame.width }
                leadingConstraints[i].constant = topOffset
            }
        }
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.startAnimation()
        } else {
            action()
        }
    }
    
    func remove(at index: Int, animated: Bool) {
        guard !contentView.subviews.isEmpty,
              index >= 0 && index < contentView.subviews.count else {
                  return
              }
        
        let removeViewLeadingConstraint = leadingConstraints.remove(at: index)
        let leadingConstant = removeViewLeadingConstraint.constant
        
        let removeView = contentView.subviews[index]
        removeView.removeFromSuperview()
        widthConstraints .remove(at: index)
        
        for i in 0 ..< leadingConstraints.count {
            if i == 0 {
                leadingConstraints[i].constant = 0
            } else {
                let topOffset = contentView.subviews[0 ..< i].reduce(0) { $0 + $1.frame.width }
                leadingConstraints[i].constant = topOffset
            }
        }
        
        removingContentView.addSubview(removeView)
        removeView.topAnchor.constraint(equalTo: removingContentView.topAnchor).isActive = true
        removeView.bottomAnchor.constraint(equalTo: removingContentView.bottomAnchor).isActive = true
        let newRemoveViewLeadingConstraint = removeView.leadingAnchor.constraint(equalTo: removingContentView.leadingAnchor, constant: leadingConstant)
        newRemoveViewLeadingConstraint.isActive = true
        newRemoveViewLeadingConstraint.constant -= removeView.frame.width
        
        trailingConstraint?.isActive = false
        if let lastView = contentView.subviews.last {
            let trailingConstraint = lastView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
            trailingConstraint.isActive = true
            self.trailingConstraint = trailingConstraint
        }
        
        let action = {
            self.scrollView.layoutIfNeeded()
            self.scrollView.invalidateIntrinsicContentSize()
            self.layoutView.superview?.layoutIfNeeded()
        }
        
        let completion = {
            removeView.removeFromSuperview()
        }
        
        if animated {
            animator.addAnimations {
                action()
            }
            animator.addCompletion { _ in
                completion()
            }
            
            animator.startAnimation()
        } else {
            action()
            completion()
        }
    }
    
    func layoutSubviews() {
    }
    
    func willRemoveFromSuperview() {
        maxSizeConstraint?.isActive = false
        widthConstraints.forEach { $0.isActive = false }
    }
    
    private func arrangeLayerZ(views: [UIView]) {
        var count = views.count
        views.forEach {
            $0.layer.zPosition = CGFloat(count)
            count -= 1
        }
    }
}
