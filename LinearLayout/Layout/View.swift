//
//  View.swift
//  LinearLayout
//
//  Created by Tony Wu(吳少華) on 2022/1/18.
//

import Foundation
import UIKit

class View: UIView {
    var isDynamic = false
    override var intrinsicContentSize: CGSize {
        if isDynamic {
            return frame.size
        } else {
            return super.intrinsicContentSize
        }
    }
}
